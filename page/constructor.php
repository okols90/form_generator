<?include "header.php";?>
    <div class="container">
        <?
        ob_start();
        echo "Конструктор форм";
        $title = ob_get_contents();
        ob_end_clean();
        ?>
        <h1><?=$title?></h1>
        <div class="row">
            <div class="col-md-3">
                <?include "sidebar.php";?>
            </div>
            <div class="col-md-9  border border-primary">
                <div class="constr" id="newConst">
                    <div class="block_add_form block_add_form_name">
                        Введите название форму
                        <input type="text" class="form-control"  name=""><hr/>
                    </div>

                    <div class="block_add_form block_add_form_field form-group" id="addBlock">
                        Введите название поле
                        <input type="text" class="form-control input_name" name="" data-name="">

                        Выберите тип поле
                        <select class="form-control input_type">
                            <option value="input">Строка (input)</option>
                            <option value="textarea">Текст (textarea)</option>
                        </select>

                        <input type="checkbox" class="form-check-label input_req" id="req" name="req" value="">
                        <label for="req">Обязательно поле</label>
                        <hr/>
                    </div>
                </div>
                <button type="button" class="btn btn-primary form-group" id="butAddBlock">Добавить поле</button>
                <button type="button" class="btn btn-success form-group" id="butSaveBlock">Сохранить форму</button>
                <?

                ?>
                <div id="results"></div>
            </div>
        </div>
    </div>
<?include "footer.php";?>