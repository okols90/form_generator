<?include "header.php";?>
<div class="container">
    <?
    ob_start();
    echo "404";
    $title = ob_get_contents();
    ob_end_clean();
    ?>
    <h1><?=$title?></h1>
    <div class="row">
        <div class="col-md-3">
            <?include "sidebar.php";?>
        </div>
        <div class="col-md-9  border border-primary">
            <ul>
                <li>Введен ошибочный адрес страницы (или пользователь использует некорректную ссылку).</li>
                <li>Страница, запрашиваемая по ссылке, удалена (например, удалена ОО, удален профиль пользователя, удалена группа и т.п.).</li>
                <li>Убедиться, что в ссылке нет лишних знаков (например, точки в конце ссылки).</li>
                <li>Если ссылка получена от другого пользователя - попросить пользователя проверить корректность ссылки еще раз</li>
            </ul>
        </div>
    </div>
</div>
<?include "footer.php";?>
