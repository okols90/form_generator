<?include "header.php";?>
<div class="container">
    <?
        ob_start();
        echo "Список форм";
        $title = ob_get_contents();
        ob_end_clean();

        GLOBAL $db;
    ?>
    <h1><?=$title?></h1>
    <div class="row">
        <div class="col-md-3">
            <?include "sidebar.php";?>
        </div>
        <div class="col-md-9  border border-primary">
            <?if(!isset($_GET['formid'])){?>
                <ul class="list-group list-group-flush">
                    <?=$db->form_list();?>
                </ul>
            <?}else{?>
                <form method="post" id="formSend" data-form="<?=$_GET['formid']?>">
                    <?=$db->form_one($_GET['formid']);?>
                </form>
            <?}?>

            <div id="results"></div>
        </div>
    </div>
</div>
<?include "footer.php";?>
