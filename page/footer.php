<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2019
 * Time: 16:45
 */
?>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    $('html').on('click', "#butAddBlock", function (e) {
        e.preventDefault();
        var block = $('#addBlock').html();
        $('#newConst').append("<div class='block_add_form block_add_form_field form-group'>"+block+"</div>");
    });

    $('html').on('click', "#butSaveBlock", function (e) {
        e.preventDefault();
        var nameForm = $('.block_add_form_name input').val();
        var fieldAll = $('.block_add_form_field');
        var field = [];
        $.each(fieldAll,function (index,value) {
            field.push({"NAME": $(value).find('.input_name').val(), "TYPE": $(value).find('.input_type').val(), "REQ": $(value).find('.input_req')[0].checked});
        });
        if(nameForm.length > 0) {
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                cache: false,
                data: {"nameForm": nameForm, "fieldForm": field},
                dataType: "text",
                success: function (data) {
                    console.log(data);
                    $('#results').html(JSON.parse(data));
                },
                beforeSend: function (data) {
                    $('#results').html('<p>Ожидание данных...</p>');
                },

                error: function (data) {
                    $('#results').html('<p>Возникла неизвестная ошибка. Пожалуйста, попробуйте чуть позже...</p>');
                }
            });
        }else {
            $('#results').html('<p>Есть незаполненые поля.</p>');
        }

    });

    $('html').on('submit', "#formSend", function (e) {
        e.preventDefault();
        var thin = $(this);
        var sendMail = $('#sendMail').val();
        var title = thin.find('h3.title').text();
        var formID = thin.data("form");
        var fieldAll = $('.block_add_form_field input, .block_add_form_field textarea');
        var field = [];
        $.each(fieldAll,function (index,value) {
            field.push({"NAME": $(value).parents('.block_add_form_field').find('label').text(), "DATA": $(value).val()});
        });
        $.ajax({
            type: 'POST',
            url: '../page/send.php',
            cache: false,
            data: {"submit": "Y", "sendMail": sendMail, "formID": formID, "title": title,"field": field},
            dataType: "text",
            success: function (data) {
                console.log(data);
                $('#results').html(JSON.parse(data));
            },
            beforeSend: function (data) {
                $('#results').html('<p>Ожидание данных...</p>');
            },
            error: function (answer, status, e){
                alert("Error " + e);
            }

        });
    });
</script>
</body>
</html>
