<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2019
 * Time: 11:26
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("DROOT", $_SERVER['DOCUMENT_ROOT']);

spl_autoload_register(function ($class_name) {
    $class = DROOT."/form/".str_replace("\\", "/", $class_name) . '.php';
    require_once $class;
});

use \lib\req\REQUEST;
use \lib\db\DB;

$page = new REQUEST();
$db = new DB();

function deb($str = ""){
    echo "<pre>";
    print_r($str);
    echo "</pre>";
}