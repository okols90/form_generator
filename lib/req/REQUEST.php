<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2019
 * Time: 11:17
 */

namespace lib\req;

class REQUEST
{
    public $reqStr = [];
    public $url = "";
    public $arg = "";

    public function __construct(){
        $newUri = str_replace("/form/", "", $_SERVER['REQUEST_URI']);
        $newUri = explode("?", $newUri);
        $this->reqStr = $newUri;
        $this->url  = explode("/", $this->reqStr[0]);
        if(isset($this->reqStr[1]))
            $this->arg  = explode("&", $this->reqStr[1]);
    }

    public function rend(){

    }

}