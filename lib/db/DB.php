<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2019
 * Time: 11:17
 */

namespace lib\db;

use mysql_xdevapi\Result;

class DB
{
    public $dbh;

    public function __construct()
    {
        try {
            $this->dbh = new \PDO('mysql:host=localhost;dbname=fantasty_form', 'fantasty_form', 'fantasty_form', array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function form_list()
    {
//        $this->dbh->query('CREATE TABLE form_list (
//            id int (10) AUTO_INCREMENT,
//            name varchar(20) NOT NULL,
//            PRIMARY KEY (id)
//        )');
//
//        $this->dbh->query('CREATE TABLE field_list (
//            id int (10) AUTO_INCREMENT,
//            name varchar(20) NOT NULL,
//            type varchar(20) NOT NULL,
//            req int(1) NOT NULL,
//            id_form int(10) NOT NULL,
//            PRIMARY KEY (id)
//        )');

//        $this->dbh->query('CREATE TABLE send_list (
//            id int (10) AUTO_INCREMENT,
//            text text NOT NULL,
//            id_form int(10) NOT NULL,
//            PRIMARY KEY (id)
//        )');

        foreach ($this->dbh->query('SELECT * from form_list') as $row) {
            echo "<li class='list-group-item'><a href='?formid={$row['id']}'>{$row['name']}</a></li>";
        }
        $this->dbh = null;
    }


    public function form_one($id = "")
    {
        if (isset($id) && $id !== "") {
            foreach ($this->dbh->query('SELECT * from form_list WHERE id =' . $id) as $row) {
                echo "<h3 class='title'>Форма: {$row['name']}</h3>";
            }

            echo "<div class='form-group row'>";
            echo "<label for='sendMail' class='col-sm-2 col-form-label'>Кому отправить</label>";
            echo "<div class='col-sm-10'>";
            echo "<input type='email' class='form-control' required id='sendMail' name='sendMail' value='okolesnikov90@gmail.com'>";
            echo "</div>";
            echo "</div>";

            foreach ($this->dbh->query('SELECT * from field_list WHERE id_FORM =' . $id) as $row) {
                switch ($row['type']) {
                    case "input" :
                        {
                            $req = "";
                            if ($row['req'] == 1) $req = "required";

                            echo "<div class='form-group block_add_form_field row'>";
                            echo "<label for='{$row['type']}{$row['id']}' class='col-sm-2 col-form-label'>{$row['name']}</label>";
                            echo "<div class='col-sm-10'>";
                            echo "<input type='text' class='form-control' {$req} id='{$row['type']}{$row['id']}' name='{$row['type']}{$row['id']}'>";
                            echo "</div>";
                            echo "</div>";
                            break;
                        }
                    case "textarea" :
                        {
                            $req = "";
                            if ($row['req'] == 1) $req = "required";
                            echo "<div class='form-group block_add_form_field row'>";
                            echo "<label for='{$row['type']}{$row['id']}' class='col-sm-2 col-form-label'>{$row['name']}</label>";
                            echo "<div class='col-sm-10'>";
                            echo "<textarea class='form-control'  {$req} id='{$row['type']}{$row['id']}' name='{$row['type']}{$row['id']}'></textarea>";
                            echo "</div>";
                            echo "</div>";
                            break;
                        }
                }
            }
            echo "<button type='submit' class='btn btn-primary form-group' id='butSend' name='submit'>Отправить письмо</button>";
            $this->dbh = null;
            return null;
        } else {
            return "Нет такой формы";
        }
    }

    public function send_list(){
        foreach ($this->dbh->query('SELECT * from send_list') as $row) {
            echo "<li class='list-group-item'>{$row['text']}</li>";
        }
        $this->dbh = null;
    }

    public function form_save($form = "", $field = [])
    {
        if ($form) {
            $sql = "INSERT INTO form_list (name) VALUES ('{$form}')";
            $this->dbh->query($sql);
            $formID = $this->dbh->lastInsertId();
            foreach ($field as $fil) {
                if ($fil['REQ'] === true) $req = 1;
                else  $req = 0;
                $sql = "INSERT INTO field_list (name, type, req, id_form) VALUES ('{$fil['NAME']}', '{$fil['TYPE']}', '{$req}', '{$formID}')";
                $this->dbh->query($sql);
            }
            $this->dbh = null;
            return "Форма добавилась";
        } else {
            return "Форма не добавилась";
        }
    }

    public function send_save($title, $mess, $form)
    {
        $sql = "INSERT INTO send_list (text, id_form) VALUES ('{$mess}', '{$form}')";
        $this->dbh->query($sql);
        $this->dbh = null;
        $to = $_POST['sendMail'];
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'To: Mailer <'.$to.'>'. "\r\n";
        $headers .= 'From: okols90 <okolesnikov90@gmail.com>'. "\r\n";
        $mail = mail($to, $title, $mess, $headers);
        if($mail)  return "Спасибо! Ваше письмо отправлено и сохранено.";
        else  return "Ваше письмо не отправлено.";
    }
}
